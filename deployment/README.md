# Helm Chart Deployment

This Chart using for install Deployment on Kubernetes.

## Get Helm Repository Info
```sh
 helm repo add deployment https://gitlab.com/api/v4/projects/57461258/packages/helm/stable
 helm repo update
```

## Install Helm Chart
- See configuration
```sh
helm show values deployment/deployment
```
- Create values.yaml
- Create configuration to [conf](conf/) folder (Optional)
    - Use ConfigMap as configuration store file to ***conf/configmaps***
    - Use Secret as configuration ***conf/secrets***
    
    ***Remark:*** 

    - ConfigMap name for configuration files name is ***[service name]-cm*** or ***[Helm Chart Release Name]-cm***
    - Secret Name for configuration files is ***[service name]-secret*** or ***[Helm Chart Release Name]-secret*** 
    - ConfigMap name for Environment variable name is ***[service name]-env-cm*** or ***[Helm Chart Release Name]-env-cm***
    - Secret Name for Environment variable name is ***[service name]-env-secret*** or ***[Helm Chart Release Name]-env-secret***

- Install Helm Chart
```sh
helm install [RELEASE_NAME] -n [NAMESPACE] deployment/deployment -f values.yaml
```

## Uninstall Helm Chart
```sh
helm uninstall [RELEASE_NAME] -n [NAMESPACE]
```

## Upgrade Chart
- Edit values.yaml
```sh
helm upgrade [RELEASE_NAME] -n [NAMESPACE] deployment/deployment -f values.yaml
```
